import React from "react";
import { View, StyleSheet, Text, ImageBackground } from "react-native";
import Icon from "react-native-vector-icons/Feather";
import moment from "moment";
import Card from "./Card";

export default ({article}) => {
    return (
        <Card style={styles.view}>
            <View style={styles.horizontal}>
                <View style={styles.imageFill}>
                    <ImageBackground
                        style={styles.image}
                        resizeMode="stretch"
                        source={{ uri: article.data.featured_image.url }}
                    >

                    </ImageBackground>
                </View>
                <View style={styles.titleWrapper}>
                    <Text style={styles.title}>
                        {article.data.title}
                    </Text>
                    <Text style={styles.time}>
                        {moment(article.data.published_date).fromNow()}
                    </Text>
                </View>
            </View>
            <View style={styles.horizontal}>
                <View style={{ flex: 1 }}>
                    <Text numberOfLines={2}>
                        {decodeURIComponent(article.data.description)}
                    </Text>
                </View>
                <View style={styles.shareIcon}>
                    <Icon name="share-2" color="#000000" size={16} />
                </View>
            </View>
        </Card>
    );
};

const styles = StyleSheet.create({
    horizontal: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: 5,
    },
    view: {
        flexDirection: "column",
        marginVertical: 10,
        marginHorizontal: 5,
        paddingHorizontal: 10,
        justifyContent: "center",
    },
    titleWrapper: {
        flex:1,
        flexDirection: "column",
        marginLeft: 10,
        justifyContent: "space-between",
    },
    shareIcon: {
        alignSelf: "center",
        marginHorizontal: 5,
    },
    title: {
        fontSize: 16,
        fontWeight: "bold"
    },
    imageFill:{
        flex:1,
        flexDirection: "row"
    },
    time: {
        fontSize: 14,
        color: "#333333"
    },
    image: {
        aspectRatio: 1.5,
        flex:1,
        alignItems: "center",
        justifyContent: "center",
        marginBottom: 5,
    }
});