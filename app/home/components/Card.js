import React from "react";
import { View, StyleSheet } from "react-native";
import CardView from 'react-native-cardview';

export default ({ style, children }) => (
    <CardView
        cardElevation={2}
        cardMaxElevation={2}
        style={[mainStyle.card,style]}>
        {children}
    </CardView>
);

const mainStyle = StyleSheet.create({
    card: {
        marginHorizontal: 24,
        marginTop: 5,
        backgroundColor: "#ffffff",
        paddingHorizontal:10,
        paddingVertical: 5,
    }
});