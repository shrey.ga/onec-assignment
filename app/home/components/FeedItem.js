import React from "react";
import { View, Text } from "react-native";
import Video from "./Video";
import Article from "./Article";

export default ({ feedItem }) =>  feedItem.type === "YOUTUBE" ?  <Video video={feedItem} /> : <Article article={feedItem} />