import React from "react";
import { View, StyleSheet, Text, ImageBackground, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/Feather";
import moment from "moment";
import Card from "./Card";

export default ({video}) => {
    return (
        <Card style={styles.view}>
            <Text style={styles.title}>
                {video.data.title}
            </Text>
            <View style={styles.imageFill}>
                <ImageBackground
                    style={styles.image}
                    resizeMode="stretch"
                    source={{ uri: video.data.featured_image.url }}
                >
                    <View style={styles.playCircle}>
                        <Icon name="play" size={20} color="#ffffff" />
                    </View>
                </ImageBackground>
            </View>
            <Text style={styles.time}>
                {moment(video.data.published_date).fromNow()}
            </Text>
            <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 1, }}>
                    <Text numberOfLines={2} style={styles.description}>
                        {video.data.description}
                    </Text>
                </View>
                <TouchableOpacity style={styles.shareIcon}>
                    <Icon name="share-2" color="#000000" size={16} />
                </TouchableOpacity>
            </View>
        </Card>
    );
};

const styles = StyleSheet.create({
    view: {
        flexDirection: "column",
        marginVertical: 10,
        marginHorizontal: 5,
        paddingHorizontal: 10,
        justifyContent: "center",
    },
    shareIcon: {
        marginHorizontal: 5,
    },
    title: {
        marginTop: 5,
        fontSize: 18,
        fontWeight: "bold",
        marginBottom: 5,
    },
    imageFill:{
        flexDirection: "row"
    },
    time: {
        fontSize: 12,
        color: "#333333"
    },
    description: {
        fontSize: 14,
        color: "#000000",
    },
    image: {
        aspectRatio: 1.5,
        flex:1,
        alignItems: "center",
        justifyContent: "center",
        marginBottom: 5,
        marginTop: 10
    },
    playCircle: {
        height: 40,
        width: 40,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "rgba(0,0,0,0.7)",
        borderRadius: 20,
    }
});