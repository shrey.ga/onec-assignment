import React from "react";
import { View, Text, ImageBackground, StyleSheet, Dimensions, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/EvilIcons";

export default ({ liveEvent }) => (<ImageBackground resizeMode="stretch" source={{ uri: liveEvent.creatives.bannerUpcoming.url }} style={styles.image}>
    <View style={styles.bottombar}>
        <TouchableOpacity style={styles.btnPrelim}>
                <Text style={{ color: "#ffffff", fontSize: 18 }}>PRELIMS</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.btnCard}>
            <Text style={{ color: "#000000", fontSize: 18 }}>Main Card</Text>
            <Icon name="external-link" size={26} color="#000000" />
        </TouchableOpacity>
    </View>
</ImageBackground>);

const buttonHeight = Dimensions.get("window").width*0.120;
const styles = StyleSheet.create({
    image: {
        width: Dimensions.get("window").width,
        height: Dimensions.get("window").width*0.506,
        flexDirection: "column-reverse",
        alignItems: "center"
    },
    bottombar: {
        position: "absolute",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        bottom: 0,
        left: 0,
        right: 0,
        height: Dimensions.get("window").width*0.145,
    },
    btnPrelim: {
        marginEnd: 20,
        height: buttonHeight,
        borderColor: "#ffffff",
        padding: 10,
        paddingHorizontal:20,
        borderWidth: 3,
        borderRadius: 2,
        alignItems: "center",
        justifyContent: "center"
    },
    btnCard: {
        height: buttonHeight,
        backgroundColor: "#e6e6e6",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        padding: 10,
        borderWidth: 2,
        paddingHorizontal:20,
        borderRadius: 2,
        alignItems: "center",
        justifyContent: "center"
    },
});