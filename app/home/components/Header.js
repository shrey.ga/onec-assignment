import React from "react";
import { View, StyleSheet, Text, Platform, SafeAreaView } from "react-native";
import Icon from "react-native-vector-icons/Feather";
const APPBAR_HEIGHT = Platform.select({
    ios: 44,
    android: 56,
  });

const styles = StyleSheet.create({
    containerStyle: {
        backgroundColor: "#000000",
        height: APPBAR_HEIGHT,
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: "space-between",
    }
});

export default () => (
    <SafeAreaView style={{ backgroundColor: "#000000" }}>
        <View style={styles.containerStyle}>
            <View style={{ marginLeft: 10 }}>
               <Icon name="menu" size={22} color="#fff" />
            </View>
            <View>
                <Text style={{ color: "#ffffff", fontSize: 18 }}>ONE</Text>
            </View>
            <View style={{ marginEnd: 10 }}>
               <Icon name="search" size={22} color="#fff" />
            </View>
        </View>
    </SafeAreaView>
);
