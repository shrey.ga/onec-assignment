import React from "react";
import { View, Text, FlatList } from "react-native";
import PropTypes from "prop-types";
import FeedItem from "./FeedItem";
import LiveEvent from "./LiveEvent";

export default ({ layoutStore }) => {
    return (
        <FlatList
            style={{ flex:1 }}
            data={layoutStore.feed}
            keyExtractor={(item) => item.id}
            renderItem={({ item }) => <FeedItem feedItem={item}  />}
            ListHeaderComponent={() => layoutStore.upcomingEvent ? <LiveEvent liveEvent={layoutStore.upcomingEvent} /> : null}
        />
    )
}