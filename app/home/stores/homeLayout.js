import { decorate, computed, flow, observable } from "mobx";
import { AsyncStorage, NetInfo } from "react-native";
import fetch from "../../fetch";

class HomeLayout {
    constructor() {
        this.loadLayout = flow(this.loadLayout);
        this.loadCached = flow(this.loadCached);
        this.loading = false;
        this.loaded = false;
        this.loadingError = false;
        this.upcomingEvent = null;
        this.cacheKey = "homeLayout";
        this.feed = [];
    }

    * loadCached() {
        try {
            let layout = yield AsyncStorage.getItem(this.cacheKey);
            if (layout) {
                layout = JSON.parse(layout);
                if (layout) {
                    this.upcomingEvent = layout.upcomingEvent;
                    this.feed = layout.feed;
                    return true;
                }
            }
        } catch(e) {
            alert(e);
        }
        return false;
    }

    *loadLayout() {
        try {
            this.loading = true;
            // if cached, load from remote server sliently.
            const isCached = yield this.loadCached();
            if (isCached === true) {
                this.loaded = true;
                this.loading = false;
            }
            // can be improved for failed cases. If one request fails. Other should till we shown
            // depends on business requirements
            const response = yield Promise.all([fetch("event"), fetch("lpfeed")]);
            const caching = {};
            if (response[0].data.length > 0) {
                this.upcomingEvent = response[0].data[0];
                caching.upcomingEvent = response[0].data[0];
            }
            this.feed = response[1].data;
            caching.feed = response[1].data;
            this.loaded = true;
            this.loading = false;
            yield AsyncStorage.setItem(this.cacheKey, JSON.stringify(caching));
        } catch(e) {
            alert(e);
            this.loaded = true;
            this.loadingError = true;
            this.loading = false;
        }
    }

}

decorate(HomeLayout, {
    loaded: observable,
    loading: observable,
    loadingError: observable,
})

export default new HomeLayout();