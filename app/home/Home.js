import React, { Component } from "react";
import { observer } from "mobx-react-lite";
import { View, ActivityIndicator, StyleSheet, Text, SafeAreaView } from "react-native";
import HomeLayout from "./components/HomeLayout";
import homeLayoutStore from "./stores/homeLayout";
import Header from "./components/Header";

// gets mobx store
const HomeLayoutContext = React.createContext(homeLayoutStore);

export default observer((props) => {
    const layoutStore = React.useContext(HomeLayoutContext);
    React.useEffect(() =>{
        if (layoutStore.loaded === false && layoutStore.loading === false) {
            layoutStore.loadLayout();
        }
    });
    return (
        <View style={{ flex: 1, }}>
            <Header />
            <View style={styles.screen}>
                { layoutStore.loading ? (<View style={styles.alignCenter}>
                    <ActivityIndicator size="large" animating />
                </View>) : null}
                { layoutStore.loaded && !layoutStore.loadingError ? <HomeLayout layoutStore={layoutStore} /> : null }
                { layoutStore.loaded && layoutStore.loadingError ? (
                    <View style={styles.alignCenter}>
                        <Text onPress={layoutStore.loadLayout}>
                            Error in loading. Please try again
                        </Text>
                    </View>
                ) : null}
            </View>
        </View>
    )
});

const styles = StyleSheet.create({
    screen: {
        backgroundColor: "#E6E6E6",
        flex:1,
    },
    alignCenter: {
        flex:1,
        justifyContent: "center",
        alignItems: "center"
    }
})