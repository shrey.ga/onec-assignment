const BASE_URL = "https://app.onefc.com/api/v4/";

export default (url, ...args) => fetch(BASE_URL+url,...args).then(response => response.json());